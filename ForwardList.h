// Create a custom singly linked list class that would support all the
// functionality given in main.cpp.

// Again: Declare your functions inside your class, but define them outside of
// it.

#include <cstddef>
#include <iostream>

template <typename T> struct Node {
  T value;
  Node<T> *next = nullptr;
};

template <typename T> class ForwardList {
private:
  Node<T> *m_start = nullptr;

public:
  class iterator {
  public:
    // Iterator tags here...
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using value_type = T;
    using pointer = T *;   // or also value_type*
    using reference = T &; // or also value_type&

    // Iterator constructors here...
    iterator(Node<T> *ptr) : m_ptr(ptr) {}

    // reference
    T &operator*() const { return m_ptr->value; }
    // pointer
    T *operator->() { return &(m_ptr->value); }

    // Prefix increment
    iterator &operator++() {
      m_ptr = m_ptr->next;
      return *this;
    }

    // Postfix increment
    iterator operator++(int) {
      iterator tmp = *this;
      m_ptr = m_ptr->next;
      return tmp;
    }

    friend bool operator==(const iterator &a, const iterator &b) {
      return a.m_ptr == b.m_ptr;
    };
    friend bool operator!=(const iterator &a, const iterator &b) {
      return a.m_ptr != b.m_ptr;
    };

  private:
    Node<T> *m_ptr;
  };
  ForwardList();
  ForwardList(const ForwardList &obj);
  ForwardList(size_t size);
  ForwardList(size_t size, T &elem);
  ForwardList(size_t size, T &&elem);
  ~ForwardList();

  bool empty();
  T &front();
  const T &front() const;
  void push_front(T &elem);
  void push_front(T &&elem);
  void pop_front();
  void push_back(T &elem);
  size_t size();
  template <typename... Args> void emplace_front(Args &&...args);
  void clear();
  ForwardList::iterator begin();
  ForwardList::iterator end();

  bool operator==(const ForwardList &lst);
  bool operator!=(const ForwardList &lst);
  ForwardList &operator=(const ForwardList &list);
};

// constructors
template <typename T> ForwardList<T>::ForwardList() {}

template <typename T> ForwardList<T>::ForwardList(const ForwardList &obj) {
  m_start = new Node<T>{.value = obj.m_start->value, .next = nullptr};
  Node<T> *prev_pt = m_start;
  Node<T> *next_pt = nullptr;
  Node<T> *obj_pt = obj.m_start->next;
  while (obj_pt != nullptr) {
    next_pt = new Node<T>{.value = obj_pt->value};
    prev_pt->next = next_pt;

    obj_pt = obj_pt->next;
    prev_pt = next_pt;
    next_pt = nullptr;
  }
}

template <typename T> ForwardList<T>::ForwardList(size_t size) {
  m_start = new Node<T>;
  Node<T> *current = m_start;
  for (size_t i = 0; i < size - 1; ++i) {
    Node<T> *new_node = new Node<T>;
    current->next = new_node;
    current = new_node;
  }
}

template <typename T> ForwardList<T>::ForwardList(size_t size, T &elem) {
  m_start = new Node<T>{.value = elem, .next = nullptr};
  Node<T> *current = m_start;
  for (size_t i = 0; i < size - 1; ++i) {
    Node<T> *new_node = new Node<T>{.value = elem, .next = nullptr};
    current->next = new_node;
    current = new_node;
  }
}

template <typename T> ForwardList<T>::ForwardList(size_t size, T &&elem) {
  m_start = new Node<T>{.value = elem, .next = nullptr};
  Node<T> *current = m_start;
  for (size_t i = 0; i < size - 1; ++i) {
    Node<T> *new_node = new Node<T>{.value = elem, .next = nullptr};
    current->next = new_node;
    current = new_node;
  }
}

template <typename T> ForwardList<T>::~ForwardList() {
  Node<T> *pt = m_start;
  while (pt != nullptr) {
    Node<T> *next = pt->next;
    delete pt;
    pt = next;
  }
}

// methods
template <typename T> bool ForwardList<T>::empty() {
  return m_start == nullptr;
}

template <typename T> T &ForwardList<T>::front() { return m_start->value; }

template <typename T> const T &ForwardList<T>::front() const {
  return m_start->value;
}

template <typename T> void ForwardList<T>::push_front(T &elem) {
  Node<T> *old_start = m_start;
  m_start = new Node<T>{.value = elem, .next = old_start};
}

template <typename T> void ForwardList<T>::push_front(T &&elem) {
  Node<T> *old_start = m_start;
  m_start = new Node<T>{.value = elem, .next = old_start};
}

template <typename T> void ForwardList<T>::pop_front() {
  Node<T> *new_start = m_start->next;
  delete m_start;
  m_start = new_start;
}

template <typename T> size_t ForwardList<T>::size() {
  Node<T> *pt = m_start;
  size_t size = 0;
  while (pt != nullptr) {
    ++size;
    pt = pt->next;
  }
  return size;
}
template <typename T> void ForwardList<T>::push_back(T &elem) {
  Node<T> *current = m_start;
  while (m_start->next != nullptr) {
    current = current->next;
  }

  Node<T> *new_node = new Node<T>{.value = elem, .next = nullptr};
  current->next = new_node;
}

template <typename T>
template <typename... Args>
void ForwardList<T>::emplace_front(Args &&...args) {
  Node<T> *old_start = m_start;
  m_start =
      new Node<T>{.value = T(std::forward<Args>(args)...), .next = old_start};
}

template <typename T> void ForwardList<T>::clear() {
  Node<T> *pt = m_start;
  while (pt != nullptr) {
    Node<T> *next = pt->next;
    delete pt;
    pt = next;
  }
  m_start = nullptr;
}

template <typename T> typename ForwardList<T>::iterator ForwardList<T>::begin() {
  return ForwardList<T>::iterator(m_start);
}

template <typename T> typename ForwardList<T>::iterator ForwardList<T>::end() {
  return ForwardList<T>::iterator(nullptr);
}

// operators
template <typename T> bool ForwardList<T>::operator==(const ForwardList &lst) {
  Node<T> *pt = m_start;
  Node<T> *pt2 = lst.m_start;
  while (pt != nullptr && pt2 != nullptr) {
    if (pt->value != pt2->value) {
      return false;
    }
    pt = pt->next;
    pt2 = pt2->next;
  }
  return pt == nullptr && pt2 == nullptr;
}

template <typename T> bool ForwardList<T>::operator!=(const ForwardList &lst) {
  return !(*this == lst);
}
template <typename T>
ForwardList<T> &ForwardList<T>::operator=(const ForwardList &obj) {
  m_start = new Node<T>{.value = obj.m_start->value, .next = nullptr};
  Node<T> *prev_pt = m_start;
  Node<T> *next_pt = nullptr;
  Node<T> *obj_pt = obj.m_start->next;
  while (obj_pt != nullptr) {
    next_pt = new Node<T>{.value = obj_pt->value};
    prev_pt->next = next_pt;

    obj_pt = obj_pt->next;
    prev_pt = next_pt;
    next_pt = nullptr;
  }

  return *this;
}
